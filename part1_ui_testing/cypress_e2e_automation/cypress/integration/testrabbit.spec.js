///// <reference types="cypress" />
import {generate_random_date} from 'C:/Users/SKT-DELL/Documents/cypress-test/cypress_e2e_automation/cypress/support/commands.js'

before(function () {
    cy.fixture('testdata').then(function (testdata) {
        this.testdata = testdata
    })
})

describe('TC1', function() {
    
    it('IPD/OPD : step Insurance', function (){    
        cy.visit('/en/product/health-insurance/questions')
        cy.contains('IPD/OPD').click()
        cy.contains('Which IPD/OPD plan do you want to be covered?')
        //cy.get('#product_ipdopd_subcategory > :nth-child(2) > .row').children().should('have.length', 6)
        cy.contains('Salary man').click()
        cy.wait(300)
    }) 

    it('IPD/OPD : step Personal', function (){
        cy.contains('Personal')
        cy.contains('What is your phone number?')
        cy.get('input[type=tel]').type(this.testdata.phonenumber)
        cy.get('#customer_phone > :nth-child(2) > .col-12 > .btn').click() 
        cy.wait(300)
        cy.contains('What is your first name?')
        cy.contains('What is your last name?')
        cy.get('input[name=customer_first_name]').type(this.testdata.firstname)
        cy.get('input[name=customer_last_name]').type(this.testdata.lastname)
        cy.get(':nth-child(3) > .col-12 > .btn').click() 
        cy.wait(300)
        cy.contains('What is your email?')
        cy.get('#email').type(this.testdata.email)
        cy.get('#customer_email > :nth-child(2) > .col-12 > .btn').click() 
        cy.wait(300)
        cy.contains('What is your gender?') 
        cy.get('#customer_gender > :nth-child(2) > .row').children().should('have.length', 2)
        cy.contains('Female').click()
        cy.contains('What is your date of birth?')
        let RandomDate = generate_random_date(0)
        cy.get('input[name=customer_dob]').type(16051994)
        cy.get('#customer_dob > :nth-child(2) > .col-12 > .btn').click() 
        cy.wait(300)
    })  

    it('IPD/OPD : step Quotes', function (){
        cy.contains('Consent').and('be.visible')
        cy.contains('No consent').and('be.visible').click()
        cy.contains('Show Quotes').and('be.visible').click()
    })  
})


describe('TC2', function() {

    it('Specific Disease : step Insurance', function (){  
        cy.visit('/en/product/health-insurance/questions')
        cy.contains('Specific Disease').click()
        cy.contains('Which specific disease do you want to be covered?')
        cy.get('#product_disease_subcategory > :nth-child(2) > .row').children().should('have.length', 6)
        cy.contains('Heart attack').click()
        cy.wait(300)
    })
    
    it('Specific Disease : step Personal', function (){
        cy.contains('Personal')
        cy.contains('What is your phone number?')
        cy.get('input[type=tel]').type(this.testdata.phonenumber)
        cy.get('#customer_phone > :nth-child(2) > .col-12 > .btn').click() 
        cy.wait(300)
        cy.contains('What is your first name?')
        cy.contains('What is your last name?')
        cy.get('input[name=customer_first_name]').type(this.testdata.firstname)
        cy.get('input[name=customer_last_name]').type(this.testdata.lastname)
        cy.get(':nth-child(3) > .col-12 > .btn').click() 
        cy.wait(300)
        cy.contains('What is your email?')
        cy.get('#email').type(this.testdata.email)
        cy.get('#customer_email > :nth-child(2) > .col-12 > .btn').click() 
        cy.wait(300)
        cy.contains('What is your gender?') 
        cy.get('#customer_gender > :nth-child(2) > .row').children().should('have.length', 2)
        cy.contains('Female').click()
        cy.contains('What is your date of birth?')
        let RandomDate = generate_random_date(0)
        cy.get('input[name=customer_dob]').type(16051994)
        cy.get('#customer_dob > :nth-child(2) > .col-12 > .btn').click() 
        cy.wait(300)
    })  

    it('Specific Disease : step Quotes', function (){
        cy.contains('Consent').and('be.visible')
        cy.contains('No consent').and('be.visible').click()
        cy.contains('Show Quotes').and('be.visible').click()
    })  
})


describe('TC3', function() {

    it('Personal Accident : step Insurance', function (){   
        cy.visit('/en/product/health-insurance/questions')
        cy.contains('Personal Accident').click()
        cy.contains('Which PA plan do you want to be covered?')
        cy.get('#product_accident_subcategory > :nth-child(2) > .row').children().should('have.length', 4)
        cy.contains('Extreme sport accidents').click()
        cy.wait(300)
    })
        
    it('Personal Accident : step Personal', function (){
        cy.contains('Personal')
        cy.contains('What is your phone number?')
        cy.get('input[type=tel]').type(this.testdata.phonenumber)
        cy.get('#customer_phone > :nth-child(2) > .col-12 > .btn').click() 
        cy.wait(300)
        cy.contains('What is your first name?')
        cy.contains('What is your last name?')
        cy.get('input[name=customer_first_name]').type(this.testdata.firstname)
        cy.get('input[name=customer_last_name]').type(this.testdata.lastname)
        cy.get(':nth-child(3) > .col-12 > .btn').click() 
        cy.wait(300)
        cy.contains('What is your email?')
        cy.get('#email').type(this.testdata.email)
        cy.get('#customer_email > :nth-child(2) > .col-12 > .btn').click() 
        cy.wait(300)
        cy.contains('What is your gender?') 
        cy.get('#customer_gender > :nth-child(2) > .row').children().should('have.length', 2)
        cy.contains('Female').click()
        cy.contains('What is your date of birth?')
        let RandomDate = generate_random_date(0)
        cy.get('input[name=customer_dob]').type(16051994)
        cy.get('#customer_dob > :nth-child(2) > .col-12 > .btn').click() 
        cy.wait(300)
    })  

    it('Personal Accident : step Quotes', function (){
        cy.contains('Consent').and('be.visible')
        cy.contains('No consent').and('be.visible').click()
        cy.contains('Show Quotes').and('be.visible').click()
    })  

})