// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
export const generate_random_string = string_length => {
    let random_string = ''
    let random_ascii
    for (let i = 0; i < string_length; i++) {
    random_ascii = Math.floor(Math.random() * 25 + 97)
    random_string += String.fromCharCode(random_ascii)
    }
    return random_string
    }

export const generate_random_date = () =>  {
    let random_date = '';
    let dd = '';
    let mm = '';
    let yy = '';
    let year = '25'
    dd+= Math.random()*(31-1)+1;
    mm+= Math.random()*(12-1)+1;
    yy+= Math.random() * (2) //Math.random()*10+1;
    //random_date += [dd + mm + '25' +yy].join('') ;
    random_date += `${dd}${mm}${yy}!`
    return random_date
    }

