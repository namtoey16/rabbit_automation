﻿** Settings ***
Library         Collections
Library         String
Library         RequestsLibrary
Library         json
Library         JSONLibrary
Library         HttpLibrary.HTTP
Library         OperatingSystem
Library           DateTime


*** Variables ***
${base_url}              https://order-pizza-api.herokuapp.com


*** Test Cases ***
PREPARE_DATA
    ${ORDER}        Generate random string    2    0123456789
    ${TABLE}        Generate random string    1    0123456789
    ${DATE} =       Get Current Date
    Set Global Variable                          ${ORDER}
    Set Global Variable                          ${TABLE}
    Set Global Variable                          ${DATE}


TC1_POST_AUTH
    Create Session      post_auth               ${base_url}         verify=True
    ${headers}=         Create Dictionary       Content-Type=application/json
    ${body}=            Create dictionary       username=test       password=test
    ${response}=        Post Request            post_auth           /api/auth               headers=${headers}          data=${body}
    ${response.status_code}                     Convert To String                           ${response.status_code}
    log to console                              status_code : ${response.status_code}
    log to console                              headers : ${response.headers}
    log to console                              body : ${response.content}
    should be equal                             ${response.status_code}                     200
    ${accessToken}=                             evaluate    $response.json().get("access_token")
    Set Global Variable                         ${accessToken}


TC2_GET_REQUEST
    Create Session      get_order               ${base_url}
    ${response}=        get request             get_order                               /api/orders
    ${response.status_code}                     Convert To String                       ${response.status_code}
    log to console                              status_code : ${response.status_code}
    log to console                              headers : ${response.headers}
    log to console                              body : ${response.content}
    should be equal                             ${response.status_code}                 200


TC3_POST_REQUEST
    Create Session      post_order              ${base_url}
    ${body}=            create dictionary       "Crust": "pizza"   "Flavor": "spicy"  "Order_ID": ${ORDER}   "Size": "XL"  "Table_No": ${TABLE}   "Timestamp": "${DATE}"
    ${headers}=         create dictionary       Authorization=Bearer ${accessToken}      Accept=application/json     Content-Type=application/json
    ${response}=        post request            post_order                      /api/orders     data=${body}         headers=${headers}
    ${response.status_code}                     Convert To String               ${response.status_code}
    log to console                              status_code : ${response.status_code}
    log to console                              headers : ${response.headers}
    log to console                              body : ${response.content}
    should be equal                             ${response.status_code}             201

TC4_DELETE_REQUEST
    Create Session      delete_order               ${base_url}
    ${response}=        delete request           delete_order                           /api/orders/${ORDER}
    ${response.status_code}                     Convert To String                       ${response.status_code}
    log to console                              status_code : ${response.status_code}
    log to console                              headers : ${response.headers}
    log to console                              body : ${response.content}
    should be equal                             ${response.status_code}                 200